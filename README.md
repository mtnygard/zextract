# Export Zettelkasten notes to standard markdown

I've started using [Neuron](https://neuron.zettel.page) to keep notes. The [Zettelkasten](https://zettelkasten.de/) style is to keep all your notes (for your whole life!) in one big file cabinet.

This doesn't mesh too well with project-based activities. So this tool lets me export a set of notes to a directory full of markdown files.

Links are translated from Neuron's format (which looks like eight hex digits inside angle brackets) into local file names.

## Running

`clj -m zextract _zetteldir_ _output-dir_ _tag1_ [_tag2_ ...]`

If multiple tags are given, then the notes must match *all* tags to be included. The tagged notes are the "roots" of the export. All notes linked from the tagged notes will also be exported. (Technically, the transitive closure of all notes reachable from the tagged notes will be included.)
