(ns zextract
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [yaml.core :as yaml]
            [graph :as g])
  (:import java.io.File))

(defn map-vals-with-keys [f m]
  (zipmap (keys m) (map f (keys m) (vals m))))

;; Inputs
;;   zettel-dir: String path to the zettelkasten
;;   tags: Vector of string, used to find the relevant zettels
;;   output-dir: String path to the location where .md files will be written

;; Read all zettels into map indexed by ID
(def separator (re-pattern File/separator))
(def suffix ".md")

(defn path->id [path]
  (str/replace
   (last (str/split path separator))
   (re-pattern suffix) ""))

(defn read-zettel [path]
  {(path->id (.getCanonicalPath path)) (slurp path)})

(defn build-index [zettels]
  (reduce merge zettels))

(defn markdown? [p]
  (str/ends-with? p suffix))

(defn markdown-file? [f]
  (markdown? (.getName f)))

(defn index? [f]
  (= "index.md" (.getName f)))

(defn zettel-item? [f]
  (and (markdown-file? f)
       (not (index? f))))

(defn all-zettels [zettel-dir]
  (let [paths (file-seq zettel-dir)
        zettels (map read-zettel (filter zettel-item? paths))]
    (build-index zettels)))

;; Filter for matches
(defn filter-values [m pred]
  (apply hash-map
         (filter (comp pred second) m)))

(defn front-matter [z]
  (let [header? #(= "---" %)
        x
        (partition-by header? (str/split-lines z))]
    (if (and (header? (ffirst x)) (header? (first (nth x 2))))
      (str/join "\n" (second x))
      "")))

(defn metadata [z]
  (yaml/parse-string (front-matter z)))

(defn tags [z]
  (:tags (metadata z)))

(defn tagged? [t z]
  (some #{t} (tags z)))

;; Build index of zid -> title
(defn title-of [id content]
  (or (:title (metadata content))
      id))

(defn zettel-titles [z]
  (map-vals-with-keys title-of z))

;; Replace ID with title
;; Replace <ID> with markdown link and title
(def inline-link #"<([0-9a-f]{8})>")

(defn id->title [titles id]
  (get titles id id))

(defn linker [titles [_ id]]
  (let [text (id->title titles id)
        target (str/replace text #" " "%20")]
    (str "[" text "](" target ".md)")))

(defn linkify [titles content]
  (str/replace content inline-link (partial linker titles)))

;; Find the transitive closure of all zettels linked from here
(defn neighbors [allz id]
  (map second (re-seq inline-link (get allz id ""))))

(defn zgraph [allz]
  (let [nodes (keys allz)]
    (g/graph nodes (partial neighbors allz))))

(defn close-over
  "Given the map of id->content and a set of ids,
   return a set of all ids reachable from the given set.
   (The return value includes the original set as well)"
  [allz ids]
  (let [close-f (partial g/transitive-closure-of (zgraph allz))]
    (reduce into ids (map close-f ids))))

;; Find links to static content so we can bring the targeted files along
(def local-link #"\[[^\]]*\]\((static/[^\)]*)\)")

(defn embedded-links [content]
 (map second (re-seq local-link content)))

;; Bringing everything together in a grand crescendo
;; 
;; Inputs
;;   zettel-dir: String path to the zettelkasten
;;   tags: Vector of string, used to find the relevant zettels
;;   output-dir: String path to the location where .md files will be written
(defn -main [zetteldir outdir & tags]
  (let [dir (File. zetteldir)
        outdir (File. outdir)
        allz (all-zettels dir)
        title-index (zettel-titles allz)
        has-all-tags? (apply every-pred (map #(partial tagged? %) tags))
        relevant-ids (set (keep (fn [[id content]]
                                  (println "checking " id (has-all-tags? content))
                                  (when (has-all-tags? content) id)) allz))
        _ (println "relevant ids: " relevant-ids)
        reachable (close-over allz relevant-ids)
        linked-files (into #{} (mapcat embedded-links (vals (select-keys allz reachable))))]
    (doseq [id reachable
            :let [outname (str (id->title title-index id) ".md")
                  outfile (File. outdir outname)
                  content (linkify title-index (get allz id))]]
      (spit outfile content))
    (doseq [f linked-files
            :let [src-file (File. dir f)
                  dest-file (File. outdir f)
                  dest-parent (.getParentFile dest-file)]]
      (when (and (some? dest-parent) (not (.exists dest-parent)))
        (.mkdirs dest-parent))
      (with-open [in (io/input-stream src-file)
                  out (io/output-stream dest-file)]
        (io/copy in out)))))