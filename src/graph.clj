(ns graph
  "Some rudimentary graph algorithms.")

;; A graph is defined as a set of nodes and a
;; function from node->nodes (called the "neighbor function")

(defn graph [nodes neighbor-fn]
  {:nodes nodes :neighbor-fn neighbor-fn})

(defn get-neighbors [g n]
  ((:neighbor-fn g) n))

(defn lazy-walk
  "Return a lazy sequence of the nodes of a graph starting a node n.  Optionally,
   provide a set of visited notes (v) and a collection of nodes to
   visit (ns)."
  ([g n]
   (lazy-walk g [n] #{}))
  ([g ns v]
   (lazy-seq (let [s (seq (drop-while v ns))
                   n (first s)
                   ns (rest s)]
               (when s
                 (cons n (lazy-walk g (concat (get-neighbors g n) ns) (conj v n))))))))

(defn transitive-closure
  [g]
  (let [nns (fn [n]
              [n (delay (lazy-walk g (get-neighbors g n) #{}))])
        nbs (into {} (map nns (:nodes g)))]
    (graph
     (:nodes g)
     (fn [n] (force (nbs n))))))

(defn transitive-closure-of
  [g n]
  (lazy-walk g (get-neighbors g n) #{}))